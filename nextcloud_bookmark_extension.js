"use strict";

const BOOKMARKS_BASE_PATH = '/index.php/apps/bookmarks/public/rest/v2/bookmark';

let bookmarkCache = {};

async function toggleBookmark(){

    let currentTab = await getCurrentTab();
    let url = currentTab.url;
    let bookmarkInfo = await getBookmark(url);
    if(!bookmarkInfo){
        let saveSuccess = await saveBookmark(url);
        if(saveSuccess) {
            updateIcon(true, currentTab)
        }
    } else {
        let removeSuccess = await removeBookmark(bookmarkInfo);
        if(removeSuccess){
            updateIcon(false, currentTab)
        }
    }


}

async function saveBookmark(url){

    let headers = await getAuthHeaders();
    headers.append("Content-Type",'application/json');
    let settings = await getSettings();
    let queryUrl = settings.server + BOOKMARKS_BASE_PATH;
    let saveBody = { url: url, title: "", description: "", tags: "", is_public: true };
    let response = await fetch(queryUrl, { headers : headers, method: "POST", body: JSON.stringify(saveBody)} );

    if(response.status !== 200){
        console.log("Failed to save bookmark: " + await response.text());
        return false;
    }

    let json = await response.json();
    if(json.status !== "success")
    if(json.data.length === 0){
        console.log("Failed to save bookmark: " + json);
        return false;
    } else {
        bookmarkCache[url] = json.item;
    }
    return true;
}


async function removeBookmark(bookmarkInfo){

    let headers = await getAuthHeaders();
    let settings = await getSettings();
    let queryUrl = settings.server + BOOKMARKS_BASE_PATH + "/" + bookmarkInfo.id;
    let response = await fetch(queryUrl, { headers : headers, method: "DELETE" } );

    if(response.status !== 200){
        console.log("Failed to delete bookmark: " + await response.text());
        return false;
    }

    let json = await response.json();
    if(json.status !== "success")
        if(json.data.length === 0){
            console.log("Failed to delete bookmark: " + json);
            return false;
        } else {
            bookmarkCache[bookmarkInfo.url] = null;
        }
    return true;
}

async function getSettings(){
    return await browser.storage.local.get(null);
}

async function updateActiveTab(){

    let currentTab = await getCurrentTab();
    let url = currentTab.url;
    let bookmark = await getBookmark(url);

    let isBookmarked = false;
    if(bookmark){
        isBookmarked = true;
    }

    updateIcon(isBookmarked, currentTab)

}

function updateIcon(isBookmarked, currentTab) {
    browser.pageAction.setIcon({
        path: isBookmarked ? {
            19: "icons/bookmarked.svg",
            38: "icons/bookmarked.svg"
        } : {
            19: "icons/not-bookmarked.svg",
            38: "icons/snot-bookmarked.svg"
        },
        tabId: currentTab.id
    });
    browser.pageAction.setTitle({
        title: isBookmarked ? 'Remove page from NextCloud Bookmarks' : 'Save page to NextCloud bookmarks',
        tabId: currentTab.id
    });
}

async function getCurrentTab(){
    let tabs = await browser.tabs.query({active: true, currentWindow: true});
    return tabs[0];
}

async function getAuthHeaders(){

    let settings = await getSettings();
    let authCombo = settings.username + ":" + settings.password;
    return new Headers({
        "Authorization": "Basic " + btoa(authCombo)
    });
}

/**
 * Get the information about the bookmark if is exists
 * @param url The url to check
 * @returns {Promise<null|*> } Returns null of the url is not bookmarked. An bookmark object otherwise
 */
async function getBookmark(url){

    if(url in bookmarkCache){
        return bookmarkCache[url];
    }

    let authHeaders = await getAuthHeaders();
    let settings = await getSettings();
    let queryUrl = settings.server + BOOKMARKS_BASE_PATH + "?url=" + url;
    let response = await fetch(queryUrl, { headers : authHeaders, method: "GET"} );

    if(response.status !== 200){
        console.log("Failed to get information from server: " + await response.text());
        return null;
    }

    let json = await response.json();
    if(json.data.length === 0){
        bookmarkCache[url] = null;
    } else {
        bookmarkCache[url] = json.data[0];
    }
    return bookmarkCache[url];
}


browser.tabs.onUpdated.addListener(updateActiveTab);

browser.pageAction.onClicked.addListener(toggleBookmark);


