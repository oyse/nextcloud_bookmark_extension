async function saveOptions(e) {
    e.preventDefault();
    await browser.storage.local.set({
        server: document.querySelector("#server").value,
        username: document.querySelector("#username").value,
        password: document.querySelector("#password").value,
    });
}

async function restoreOptions() {

    let options = await browser.storage.local.get(null);
    document.querySelector("#server").value = options.server || "";
    document.querySelector("#username").value = options.username || "";
    document.querySelector("#password").value = options.password || "";
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
